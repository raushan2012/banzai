echo "==> Uninstalling the app from the device..."
adb uninstall com.Banzai.QATestTask

echo "==> Installing the app on the device..."
adb install app/build.apk

echo "==> Setup ADB port forwarding..."
adb forward --remove-all
adb forward tcp:13000 tcp:13000

echo "==> Start the app..."
adb shell am start -n com.Banzai.QATestTask/com.unity3d.player.UnityPlayerActivity
sleep 10

echo "==> Create virtual env if it doesn't exist..."
[ -d raushan-env37 ] || python3 -m venv raushan-env37

echo "==> Activate Python virtual environment and install dependencies..."
source ./raushan-env37/bin/activate
./raushan-env37/bin/python3 -m pip install -r requirements.txt

echo "==> Run the tests..."
./raushan-env37/bin/python3 -m unittest tests.calc_test_suite.SmokeTestCase

echo "==> ALL DONE!"
