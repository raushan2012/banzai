import sys
import os

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)

import unittest
from decimal import Decimal
from typing import Dict

from altunityrunner import By

from tests.base_test import TestBase

CHARS_TO_BUTTONS: Dict[str, str] = {
    '<': 'EraseSymbol',
    'c': 'ClearEntry',
    'C': 'ClearAll',
    '±': 'ChangeSign',
    '/': 'Division',
    '*': 'Multiplication',
    '-': 'Minus',
    '.': 'Point',
    '=': 'Equals',
    '+': 'Plus',
}


class SmokeTestCase(TestBase):
    """Смоук тест, наследник основного метода TestBase, в котором прописаны сетапы и тирдауны, выполняющиеся один раз"""

    def setUp(self):
        """Перед каждым тестом очищает поле ввода"""
        self.unity_driver.find_object(By.NAME, "ClearAll").tap()

    def assert_calc_display_text(self, expected_result: str, multi_line: bool = False):
        """Экран калькулятора показывает иногда три строки, иногда одну.
        Эта функция применяет одно- или многостроковый assert в зависимости от того, какой нужен в данный момент

        :param expected_result: ожидаемый результат
        :param multi_line: если нужно многостроковое сравнение, то True (по умолчанию - False)
        """
        if multi_line:
            self.assertMultiLineEqual(expected_result, self.calculator.get_display_text())
        else:
            self.assertEqual(expected_result, self.calculator.get_display_text().strip())

    def check_buttons_pressed_sequence(self, list_of_buttons: list, expected_result: str, multi_line: bool = False):
        """Простой способ проверки нажатия последовательности кнопок

        :param list_of_buttons: последовательность нажатия кнопок в виде списка
        :param expected_result: ожидаемый результат на экране калькулятора
        :param multi_line:      нужно ли многостроковое сравнение ожидаемого результата с реальным
        """
        self.assertEqual("0", self.calculator.get_display_text().strip())

        for button in list_of_buttons:
            self.unity_driver.find_object(By.NAME, button).tap()

        self.assert_calc_display_text(expected_result=expected_result, multi_line=multi_line)

    def check_string_of_buttons(self, string_of_button_characters: str, expected_result: str, multi_line: bool = False):
        """Проверка результата последовательности нажатия кнопок, полученной в виде строки символов из CHARS_TO_BUTTONS

        :param string_of_button_characters: последовательность нажатия кнопок в виде строки символов из CHARS_TO_BUTTONS
        :param expected_result:             ожидаемый результат на экране калькулятора
        :param multi_line:                  нужно ли многостроковое сравнение ожидаемого результата с реальным
        """
        self.assertEqual("0", self.calculator.get_display_text().strip())

        for char in string_of_button_characters:
            self.unity_driver.find_object(By.NAME, char if char.isdigit() else CHARS_TO_BUTTONS[char]).tap()

        self.assert_calc_display_text(expected_result=expected_result, multi_line=multi_line)

    def check_equation(self, x: float, symbol: str, y: float, ndigits: int = 7, e: str = 'G'):
        """Функция проверяет простые уравнения преобразуя результат в экспоненциальную запись если нужно

        :param x:       первое число
        :param symbol:  символ производимой операции над числами
        :param y:       второе число
        :param ndigits: количество цифр после запятой в мантиссе при экспоненциальной записи результата
        :param e:       формат представления чисел с плавающей запятой (по умолчанию G - общий)
        """
        result = {
            '+': lambda x, y: x + y,
            '-': lambda x, y: x - y,
            '*': lambda x, y: x * y,
            '/': lambda x, y: x / y,
        }[symbol](x, y)

        self.check_string_of_buttons(string_of_button_characters=str(x) + symbol + str(y) + '=',
                                     expected_result=str('%.' + str(ndigits) + str(e)) % Decimal(str(result)))

    def test_clear_all(self):
        """Тест кнопки "C" - полного очищения результатов предыдущих вычислений"""
        self.check_buttons_pressed_sequence(list_of_buttons=list(["ClearAll"]), expected_result="0", multi_line=False)

    def test_div_by_zero(self):
        """Тест деления на ноль"""
        self.check_buttons_pressed_sequence(["1", "Division", "0", "Equals"], "Infinity")

    def test_plus_integers(self):
        """Тест прибавления двух целых чисел"""
        self.check_equation(987654321, '+', 123456789)

    def test_plus_integer_to_decimal(self):
        """Тест прибавления целого числа к десятичной дроби"""
        self.check_equation(9876543210, '+', 1234567890.0987654321)

    def test_plus_two_decimals(self):
        """Тест прибавления двух десятичных дробей"""
        self.check_equation(9876543210.0123456789, '+', 1234567890.0987654321)

    def test_minus_equation(self):
        """Тест вычитания двух чисел"""
        self.check_equation(9876543210.0123456789, '-', 1234567890.0987654321)

    def test_multiplication_equation(self):
        """Тест умножения двух чисел"""
        self.check_equation(9876543210.0123456789, '*', 1234567890.0987654321)

    def test_division_equation(self):
        """Тест деления одного числа на другое"""
        self.check_equation(9876543210.0123456789, '/', 1234567890.0987654321)

    def test_remove_last_character(self):
        """Тест кнопки удаления символов"""
        self.check_string_of_buttons(string_of_button_characters='12345<<67890<<<.098<<<<.7654321<<',
                                     expected_result='12367.76543')

    def test_zeroes_at_the_beginning(self):
        """Тест добавления нулей в начале числа"""
        self.check_string_of_buttons(string_of_button_characters='000123456789.9876543210',
                                     expected_result='123456789.9876543210')

    def test_clear_entry(self):
        """Тест кнопки "CE" - удаления последнего введенного числа """
        self.check_string_of_buttons(string_of_button_characters='1234567890.0987654321-9876543210.0123456789c',
                                     expected_result='1234567890.0987654321\n-\n0',
                                     multi_line=True)

    def test_change_the_sign(self):
        """Тест изменения знака числа"""
        self.check_string_of_buttons(string_of_button_characters='1234567890.0987654321±',
                                     expected_result='-1234567890.0987654321')

    def test_change_the_sign_two_times(self):
        """Тест изменения знака числа два раза - должно получиться то же число"""
        self.check_string_of_buttons(string_of_button_characters='1234567890.0987654321±±',
                                     expected_result='1234567890.0987654321')

    def test_point_many_times(self):
        """Тест добавления нескольких точек в число"""
        self.check_string_of_buttons(string_of_button_characters='123.456.789.987.654.3210.',
                                     expected_result='123.4567899876543210')

    def test_press_equals_two_times(self):
        """
        Тест нажатия кнопки "равно" два раза, например "5*10==" должно выдавать "500", но это на будущее
        """
        self.check_string_of_buttons(string_of_button_characters='5*10==',
                                     expected_result='50')


def suite() -> unittest.TestSuite:
    """Функция подготавливает набор тест кейсов и возвращает его

    :return: набор тест кейсов
    """
    test_suite = unittest.TestSuite()
    test_suite.addTest(SmokeTestCase.test_clear_all)
    test_suite.addTest(SmokeTestCase.test_div_by_zero)
    test_suite.addTest(SmokeTestCase.test_plus_integers)
    test_suite.addTest(SmokeTestCase.test_plus_integer_to_decimal)
    test_suite.addTest(SmokeTestCase.test_plus_two_decimals)
    test_suite.addTest(SmokeTestCase.test_minus_equation)
    test_suite.addTest(SmokeTestCase.test_multiplication_equation)
    test_suite.addTest(SmokeTestCase.test_division_equation)
    test_suite.addTest(SmokeTestCase.test_remove_last_character)
    test_suite.addTest(SmokeTestCase.test_zeroes_at_the_beginning)
    test_suite.addTest(SmokeTestCase.test_clear_entry)
    test_suite.addTest(SmokeTestCase.test_change_the_sign)
    test_suite.addTest(SmokeTestCase.test_change_the_sign_two_times)
    test_suite.addTest(SmokeTestCase.test_point_many_times)
    test_suite.addTest(SmokeTestCase.test_press_equals_two_times)
    return test_suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())
