import sys
import os

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)

import unittest

from altunityrunner import AltrunUnityDriver, os, AltUnityAndroidPortForwarding
from appium import webdriver

from tests.pages.main_page import CalcMainPage

CAPS = {
    'platformName': 'Android',
    'appPackage': 'com.Banzai.QATestTask',
    'deviceName': 'auto',
    'appActivity': 'com.unity3d.player.UnityPlayerActivity',
    'noReset': True
}


class TestBase(unittest.TestCase):
    """Базовый класс для установки соединений с драйверами аппиум и юнити"""

    @classmethod
    def setUpClass(cls):
        """Установку соединений вынес в метод класса чтобы она выполнялась один раз на весь набор тестов"""

        cls.forwarding = AltUnityAndroidPortForwarding()
        cls.forwarding.remove_all_forwards()
        cls.forwarding.forward_port_device()

        cls.appium_driver = webdriver.Remote('http://localhost:4723/wd/hub', CAPS)
        cls.wait_for_alt_unity_server()

        cls.unity_driver = AltrunUnityDriver(cls.appium_driver, 'android', log_flag=True)
        cls.calculator = CalcMainPage(cls.unity_driver)

    @classmethod
    def wait_for_alt_unity_server(cls):
        device = cls.forwarding.get_device()
        device.shell("logcat -c")
        device.shell("logcat -s Unity", handler=cls.wait_handler)

    @staticmethod
    def wait_handler(connect):
        file_obj = connect.socket.makefile()
        for line in file_obj:
            if "AltUnity Driver started" in line:
                break
        file_obj.close()
        connect.close()

    @classmethod
    def tearDownClass(cls):
        """Закрытие соединений тоже в методе класса"""
        cls.unity_driver.stop()
        cls.appium_driver.quit()
        cls.forwarding.remove_all_forwards()
