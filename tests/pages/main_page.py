import sys
import os

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)

from altunityrunner import By, WaitForObject

from tests.pages.base_page import BasePage


class CalcMainPage(BasePage):
    """Главная и пока единственная страница калькулятора (возможно, в будущем будут другие)"""

    def __init__(self, unity_driver):
        """Инициализация класса

        :param unity_driver: драйвер Unity
        """
        BasePage.__init__(self, unity_driver)

    def load(self):
        """Загрузка главной сцены"""
        self.unity_driver.load_scene('MainScene')

    @property
    def display_text(self) -> WaitForObject:
        """Находит объект "текст" на экране калькулятора и возвращает его

        :return: объект "текст" на экране калькулятора
        """
        return self.unity_driver.wait_for_object(By.NAME, value='Canvas/Display/Text', timeout=2)

    def get_display_text(self) -> str:
        """Вытаскивает значение поля "текст" из объекта "текст экрана калькулятора"

        :return: значение поля "текст"
        """
        return str(self.display_text.get_text())
