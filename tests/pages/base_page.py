import sys
import os

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(curPath)[0]
sys.path.append(rootPath)


class BasePage:
    """Основная страница на случай если у калькулятора в будущем будет несколько отображений"""

    def __init__(self, unity_driver):
        """Инициализация класса

        :param unity_driver: драйвер Unity
        """
        self.unity_driver = unity_driver